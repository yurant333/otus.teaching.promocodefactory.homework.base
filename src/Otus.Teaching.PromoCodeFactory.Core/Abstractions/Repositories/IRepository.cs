﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);
        Task<T> AddAsync(T instance);
        Task<T> UpdateAsync(T instance);
        Task<T> DeleteAsync(Guid id);
        Task<IEnumerable<T>> GetByListIdAsync(IEnumerable<Guid> Ids);
    }
}