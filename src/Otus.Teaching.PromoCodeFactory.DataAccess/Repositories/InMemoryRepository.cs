﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> AddAsync(T instance)
        {
            Data = Data.Concat(new[] {instance});
            return Task.FromResult(instance);
        }

        public Task<T> UpdateAsync(T instance)
        {
            var currentInstance = Data.FirstOrDefault(x => x.Id == instance.Id);
            if (currentInstance == null) throw new Exception("Instance not found!");
            Data = Data.Select(x => x.Id == currentInstance.Id ? currentInstance : x);
            return Task.FromResult(instance);
        }

        public Task<T> DeleteAsync(Guid id)
        {
            var currentInstance = Data.FirstOrDefault(x => x.Id == id);
            if (currentInstance == null) throw new Exception("Instance not found!");
            Data = Data.Where(x => x.Id != id);
            return Task.FromResult(currentInstance);
        }

        public Task<IEnumerable<T>> GetByListIdAsync(IEnumerable<Guid> Ids)
        {
            var result = Data.Where(x => Ids.Contains(x.Id));
            return Task.FromResult(result);
        }
    }
}