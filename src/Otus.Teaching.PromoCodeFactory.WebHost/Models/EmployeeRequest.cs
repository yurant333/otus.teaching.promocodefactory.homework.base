﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeRequest
    {
        [Required(ErrorMessage = "FirstName is required.")]
        public string FirstName { get; set; }
        
        [Required(ErrorMessage = "LastName is required.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        public string Email { get; set; }

        public List<Guid> RoleIds { get; set; }
        
        [Range(0, int.MaxValue, ErrorMessage = "Promocodes count is required! Count must be equal or more 0")]
        public int AppliedPromocodesCount { get; set; }
    }
}